<?php

namespace App\Http\Controllers;
use App\Models\Task;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function store(Request $request){
        $task = new Task();
        $task->user_id = $request->userId;
        $task->title = $request->title;
        $task->is_completed = $request->is_completed;
        $result = $task->save();

        $task_id = $task->id;

        foreach($request->tags as $tg){
            $tag = new Tag();
            $tag->task_id=$task_id;
            $tag->tag=$tg;
            $tag->save();
        }
        if($result){
            return response()->json(["result"=>"record has been saved ".$task->id]);
        }
        else{
            return response()->json(["result"=>"Operation failed"]);
        }
    }

    public function getAll(Request $request){
        $tasksByUserId = $this->getAllTasksByUserId($request->userId);
        $combinedData = $this->combineAllTagsByTaskId($tasksByUserId);
        return json_encode($combinedData);
    }

    private function getAllTasksByUserId($userId){
        $tasks = Task::where('user_id', $userId)->get();
        return $tasks;
    }

    private function combineAllTagsByTaskId($tasks){
        $taskAndTagsArr = array();
        $tagArr = array();
        foreach($tasks as $task){
            $tagsByTaskId = Tag::where('task_id', $task->id)->get();
            foreach ($tagsByTaskId as $tag) {
                $this->tagArr = array_push(
                    $tagArr,
                    array(
                        'id' => $tag->id,
                        'tag' => $tag->tag
                    )
                );
            }
            $this->taskAndTagsArr = array_push(
                $taskAndTagsArr,
                array(
                    "taskId"=>$task->id,
                    "userId"=>$task->user_id,
                    "title"=>$task->title,
                    "isCompleted"=>$task->is_completed,
                    "tags"=>$tagArr
                )
            );
            $tagArr = array();
        }
        return $taskAndTagsArr;
    }
    
    public function delete($id){
        $tag = Tag::find($id);
        $result = $tag->delete();
        if($result){
            return response()->json(["result"=>"record has been deleted ".$id]);
        }
        else{
            return response()->json(["result"=>"Operation failed"]);
        }
    }
}
