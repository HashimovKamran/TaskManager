<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\Tag;

class TaskController extends Controller
{
    // public function getAll()
    // {
    //     $tasks = Task::all();
    //     return json_encode($tasks);
    // }

    // public function store(Request $request){

    //     $request->validate([
    //         'title' => 'required',
    //         'isCompleted' => 'required'
    //     ]);

    //     try{
    //         Task::create($request->all());
    //         return $this->getAll();
    //     }catch(\Throwable $excep){
    //         return $excep->getMessage();
    //     }
    // }

    public function update(Request $request)
    {
        $task = Task::find($request->id);
        $task->title = $request->newTitle;
        $task->is_completed = $request->is_completed;
        $result = $task->update();
        if($result){
            return response()->json(["result"=>"record has been updated ".$request->id]);
        }
        else{
            return response()->json(["result"=>"Operation failed"]);
        }
    }

    public function delete($id){
        $task = Task::find($id);
        Tag::where('task_id' , '=', $task->id)->delete();

        $result = $task->delete();
        if($result){
            return response()->json(["result"=>"record has been deleted ".$id]);
        }
        else{
            return response()->json(["result"=>"Operation failed"]);
        }
    }
}
