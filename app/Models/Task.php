<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['tag_id', 'title', 'isCompleted'];

    public function tag(){
        return $this->hasMany(Tag::class,'id');
    }
}
