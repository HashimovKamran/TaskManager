<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Task;

class Tag extends Model
{
    use HasFactory;
    protected $fillable =
    [
        'task_id',
        'tag'
    ];

    public function task(){
        return $this->belongsToMany(Task::class);
    }
}
