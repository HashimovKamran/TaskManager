<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/tasks', function(){
//     return Task::all();
// });

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post("login",[UserController::class,'index']);
Route::post("register",[UserController::class,'register']);

Route::group(['middleware' => 'auth:sanctum'], function(){
    Route::post('/create', [TagController::class, 'store']);
    Route::delete('tag/delete/{id}', [TagController::class, 'delete']);
    Route::put('/update', [TaskController::class, 'update']);
    Route::delete('task/delete/{id}', [TaskController::class, 'delete']);
    Route::get('/tasks', [TagController::class, 'getAll']);
});